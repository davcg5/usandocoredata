//
//  FindOneViewController.swift
//  UsandoCoreData
//
//  Created by Laboratorio FIS on 30/1/18.
//  Copyright © 2018 DAVID. All rights reserved.
//

import UIKit

class FindOneViewController: UIViewController {

 

    var persona:Persona?
   
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = persona?.name!
        phoneLabel.text = persona?.phone
        addressLabel.text = persona?.address
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func deleteButtonPressed(_ sender: Any) {
        let manageObjectContext = (UIApplication.shared.delegate as!
        AppDelegate).persistentContainer.viewContext
        manageObjectContext.delete(persona!)
        
        do {
            try manageObjectContext.save()
        } catch  {
            print ("Error Deleting Objects")
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
