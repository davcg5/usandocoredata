//
//  FindAllViewController.swift
//  UsandoCoreData
//
//  Created by Laboratorio FIS on 30/1/18.
//  Copyright © 2018 DAVID. All rights reserved.
//

import UIKit

class FindAllViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    
    
    
var allPersons:[Persona]?  = []
var selectedPerson: Persona?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for persona in allPersons!{
            print (persona.name)
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        //numero de secciones
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allPersons!.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.allPersons![indexPath.row].name
        
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Todos"
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedPerson = allPersons![indexPath.row]
        performSegue(withIdentifier: "findAllSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "findAllSegue"{
            let destination = segue.destination as? FindOneViewController
            destination?.persona = self.selectedPerson
        }
    }
}
