//
//  ViewController.swift
//  UsandoCoreData
//
//  Created by Laboratorio FIS on 23/1/18.
//  Copyright © 2018 DAVID. All rights reserved.
//

import UIKit
import CoreData
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var addressTextField: UITextField!
    
    @IBOutlet weak var phoneTextField: UITextField!
    var singlePerson:Persona?
    var allPersons:[Persona]? = []
    let manageObjectContext = (UIApplication.shared.delegate as!
        AppDelegate).persistentContainer.viewContext
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func savePerson(){
        
        let entityDescription = NSEntityDescription.entity (forEntityName :"Persona", in: manageObjectContext)
        
        let person = Persona(entity: entityDescription!, insertInto: manageObjectContext)
        person.address = addressTextField.text ?? ""
        person.name = nameTextField.text ?? ""
        person.phone = phoneTextField.text ?? ""
        
       
        do {
            try manageObjectContext.save()
            clearFields()
        } catch  {
            print ("Error")
        }
        
    }
    func clearFields(){
        nameTextField.text = ""
        addressTextField.text = ""
        phoneTextField.text = ""
    }

   
    @IBAction func seeButtonPressed(_ sender: Any) {
   findAll()
    }
    @IBAction func findButtonPressed(_ sender: Any) {
        if nameTextField.text == "" {
            findAll()
        }
        else{
         findOne()
        }
        }
    @IBAction func saveButtonPressed(_ sender: Any) {
   savePerson()
    }
    
    
    func findAll(){
       // let entityDescription = NSEntityDescription.entity (forEntityName: "Persona", in: manageObjectContext)
        let request:NSFetchRequest<Persona> = Persona.fetchRequest()
        allPersons = []
        do {
            let results = try (manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            for result in results {
                
               let person = result as! Persona
                print ("Name: \(person.name ?? "") ", terminator: "")
                print ("Address: \(person.address ?? "") ", terminator: "")
                print ("Phone: \(person.phone ?? "") ", terminator: "")
               allPersons?.append(person)
                
            }
            performSegue(withIdentifier: "findAllSegue", sender: self)
        } catch  {
            print ("Error finding people")
        }
        
    }
    func findOne(){
        let entityDescription = NSEntityDescription.entity (forEntityName: "Persona", in: manageObjectContext)
        let request:NSFetchRequest<Persona> = Persona.fetchRequest()
        request.entity = entityDescription
        let predicate = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = predicate
        
            do {
            let results = try (manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            if results.count > 0 {
            let match = results[0] as! Persona
                singlePerson = match
                performSegue(withIdentifier: "findOneSegue", sender: self)
                print(singlePerson)
            }
                
            else {
                nameTextField.text = "n/a"
                addressTextField.text = "n/a"
                phoneTextField.text = "n/a"
                
                }
            }catch {
                print ("Error encontrado")}
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "findOneSegue"{
            
            let destination = segue.destination as! FindOneViewController
            destination.persona = self.singlePerson
        }
      
        if segue.identifier == "findAllSegue"{
            let destination = segue.destination as? FindAllViewController
            destination?.allPersons=self.allPersons
        }
}

}
